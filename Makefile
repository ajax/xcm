modules = xcomposite xdamage xfixes xrender xpresent xext x11

XCM_CFLAGS = $(shell pkg-config --cflags $(modules))
XCM_LIBS = $(shell pkg-config --libs $(modules)) 
CFLAGS ?= -O2 -g -Wall -std=c11
bindir ?= /usr/bin

.PHONY: install

xcm: xcm.c Makefile
	$(CC) $(CFLAGS) $(XCM_CFLAGS) -o xcm xcm.c $(XCM_LIBS)

install:
	install -m 0755 -t $(DESTDIR)$(bindir) xcm
