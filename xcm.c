/*
 * Copyright © 2003 Keith Packard
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Keith Packard not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  Keith Packard makes no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * KEITH PACKARD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL KEITH PACKARD BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* Modified by Matthew Hawn. I don't know what to say here so follow what it
   says above. Not that I can really do anything about it
*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/Xrender.h>
#include <X11/extensions/Xpresent.h>
#include <X11/extensions/shape.h>

typedef struct _ignore {
    struct _ignore	*next;
    unsigned long	sequence;
} ignore;

typedef struct _win {
    struct _win		*next;
    Window		id;
    Pixmap		pixmap;
    XWindowAttributes	a;
    Bool		shaped;
    Bool		damaged;
    Damage		damage;
    XserverRegion	borderSize;
    XserverRegion	extents;
    XRectangle		shape_bounds;
    union {
	struct {
	    Picture picture;
	} render;
    };
} win;

struct backend {
    void (*realize)(Display *, win *);
    void (*paint)(Display *, win *);
    void (*forget)(Display *, win *);
    void (*flip)(Display *, XserverRegion region);
};

static struct backend *backend;

static win		*list;
static int		scr;
static Window		root;
static Picture		rootPicture;
static Pixmap		rootPixmap;
static Picture		rootBuffer;
static Picture		rootTile;
static XserverRegion	allDamage;
static Bool		clipChanged;
static Bool		eventDebug;
static int		root_height, root_width;
static ignore		*ignore_head, **ignore_tail = &ignore_head;
static int		xfixes_event, xfixes_error;
static int		damage_event, damage_error;
static int		composite_event, composite_error;
static int		render_event, render_error;
static int		xshape_event, xshape_error;
static int		present_opcode, present_event, present_error;
static uint32_t		present_capabilities;
static int		composite_opcode;
static Bool		dont_grab;
static Atom		xcm_breadcrumb_atom;
static Window		breadcrumb_window;

typedef enum _compMode {
    CompAuto,
    CompRender,
    CompPresent,
} CompMode;

static CompMode compMode = CompRender;

enum _syncMode {
    SyncCore,
    SyncCmsg,
    SyncNone,
} syncMode = SyncCmsg;

static void
discard_ignore (Display *dpy, unsigned long sequence)
{
    while (ignore_head)
    {
	if ((long) (sequence - ignore_head->sequence) > 0)
	{
	    ignore  *next = ignore_head->next;
	    free (ignore_head);
	    ignore_head = next;
	    if (!ignore_head)
		ignore_tail = &ignore_head;
	}
	else
	    break;
    }
}

static void
set_ignore (Display *dpy, unsigned long sequence)
{
    ignore  *i = malloc (sizeof (ignore));
    if (!i)
	return;
    i->sequence = sequence;
    i->next = NULL;
    *ignore_tail = i;
    ignore_tail = &i->next;
}

static int
should_ignore (Display *dpy, unsigned long sequence)
{
    discard_ignore (dpy, sequence);
    return ignore_head && ignore_head->sequence == sequence;
}

static win *
find_win (Display *dpy, Window id)
{
    win	*w;

    for (w = list; w; w = w->next)
	if (w->id == id)
	    return w;
    return NULL;
}

static const char *backgroundProps[] = {
    "_XROOTPMAP_ID",
    "_XSETROOT_ID",
    NULL,
};

static Picture
root_tile (Display *dpy)
{
    Picture	    picture;
    Atom	    actual_type;
    Pixmap	    pixmap;
    int		    actual_format;
    unsigned long   nitems;
    unsigned long   bytes_after;
    unsigned char   *prop;
    Bool	    fill;
    XRenderPictureAttributes	pa;
    XRenderPictFormat *format;
    int		    p;

    pixmap = None;
    for (p = 0; backgroundProps[p]; p++)
    {
	Atom bgprop = XInternAtom (dpy, backgroundProps[p], False);

	if (XGetWindowProperty (dpy, root, bgprop, 0, 4, False,
				AnyPropertyType, &actual_type, &actual_format,
				&nitems, &bytes_after, &prop) == Success &&
	    actual_type == XInternAtom (dpy, "PIXMAP", False) &&
	    actual_format == 32 &&
	    nitems == 1)
	{
	    memcpy (&pixmap, prop, 4);
	    XFree (prop);
	    fill = False;
	    break;
	}
    }
    if (!pixmap)
    {
	pixmap = XCreatePixmap (dpy, root, 1, 1, DefaultDepth (dpy, scr));
	fill = True;
    }
    pa.repeat = True;
    format = XRenderFindVisualFormat (dpy, DefaultVisual (dpy, scr));
    picture = XRenderCreatePicture (dpy, pixmap, format, CPRepeat, &pa);
    if (fill)
    {
	XRenderColor    c;

	c.red = c.green = c.blue = 0x8080;
	c.alpha = 0xffff;
	XRenderFillRectangle (dpy, PictOpSrc, picture, &c, 0, 0, 1, 1);
    }
    else
    {
	XFreePixmap (dpy, pixmap);
    }
    return picture;
}

static void
paint_root (Display *dpy)
{
    if (!rootTile)
	rootTile = root_tile (dpy);

    XRenderComposite (dpy, PictOpSrc,
		      rootTile, None, rootBuffer,
		      0, 0, 0, 0, 0, 0, root_width, root_height);
}

static XserverRegion
win_extents (Display *dpy, win *w)
{
    XRectangle	    r;

    r.x = w->a.x;
    r.y = w->a.y;
    r.width = w->a.width + w->a.border_width * 2;
    r.height = w->a.height + w->a.border_width * 2;
    return XFixesCreateRegion (dpy, &r, 1);
}

static XserverRegion
border_size (Display *dpy, win *w)
{
    XserverRegion   border;
    /*
     * if window doesn't exist anymore,  this will generate an error
     * as well as not generate a region.  Perhaps a better XFixes
     * architecture would be to have a request that copies instead
     * of creates, that way you'd just end up with an empty region
     * instead of an invalid XID.
     */
    set_ignore (dpy, NextRequest (dpy));
    border = XFixesCreateRegionFromWindow (dpy, w->id, WindowRegionBounding);
    /* translate this */
    set_ignore (dpy, NextRequest (dpy));
    XFixesTranslateRegion (dpy, border,
			   w->a.x + w->a.border_width,
			   w->a.y + w->a.border_width);
    return border;
}

static void
destroy_window_regions (Display *dpy, win *w)
{
    if (w->borderSize)
    {
	set_ignore (dpy, NextRequest (dpy));
	XFixesDestroyRegion (dpy, w->borderSize);
	w->borderSize = None;
    }
    if (w->extents)
    {
	XFixesDestroyRegion (dpy, w->extents);
	w->extents = None;
    }
}

/* XXX w->id here is gross */
static void
render_realize_window (Display *dpy, win *w)
{
    XRenderPictureAttributes pa;
    XRenderPictFormat *format;
    Drawable draw = w->pixmap ? w->pixmap : w->id;

    pa.subwindow_mode = IncludeInferiors;
    format = XRenderFindVisualFormat (dpy, w->a.visual);
    w->render.picture = XRenderCreatePicture (dpy, draw, format,
					      CPSubwindowMode, &pa);
}

static void
present_realize_window (Display *dpy, win *w)
{
    render_realize_window (dpy, w);
}

static void
realize_window (Display *dpy, win *w)
{
    if (!w->pixmap) {
	w->pixmap = XCompositeNameWindowPixmap (dpy, w->id);

	backend->realize (dpy, w);
    }
}

static void
render_paint_window (Display *dpy, win *w)
{
    int	x, y, wid, hei;
    x = w->a.x;
    y = w->a.y;
    wid = w->a.width + w->a.border_width * 2;
    hei = w->a.height + w->a.border_width * 2;
    set_ignore (dpy, NextRequest (dpy));
    XRenderComposite (dpy, PictOpSrc, w->render.picture, None, rootBuffer,
		      0, 0, 0, 0, x, y, wid, hei);
}

static void
present_paint_window (Display *dpy, win *w)
{
    render_paint_window (dpy, w);
}

static void
render_flip (Display *dpy, XserverRegion region)
{
    XFixesSetPictureClipRegion (dpy, rootBuffer, 0, 0, region);
    XRenderComposite (dpy, PictOpSrc, rootBuffer, None, rootPicture,
		      0, 0, 0, 0, 0, 0, root_width, root_height);
}

static void
present_flip (Display *dpy, XserverRegion region)
{
    static uint32_t present_serial;
    XPresentPixmap (dpy, root, rootPixmap, present_serial++,
		    region, region, 0, 0, None, None, None,
		    None, 0, 0, 0, NULL, 0);
}

static void
paint_all (Display *dpy, XserverRegion region)
{
    XserverRegion updated;
    win	*w;

    if (!dont_grab)
	XGrabServer (dpy);

    if (!region)
    {
	XRectangle  r;
	r.x = 0;
	r.y = 0;
	r.width = root_width;
	r.height = root_height;
	region = XFixesCreateRegion (dpy, &r, 1);
    }

    updated = XFixesCreateRegion (dpy, NULL, 0);
    XFixesCopyRegion (dpy, updated, region);

    if (!rootBuffer)
    {
	XRenderPictFormat *format =
	    XRenderFindVisualFormat (dpy, DefaultVisual (dpy, scr));
	rootPixmap = XCreatePixmap (dpy, root, root_width, root_height,
				    DefaultDepth (dpy, scr));
	rootBuffer = XRenderCreatePicture (dpy, rootPixmap, format, 0, NULL);
    }
    XFixesSetPictureClipRegion (dpy, rootPicture, 0, 0, region);

    for (w = list; w; w = w->next)
    {
	/* never painted, ignore it */
	if (!w->damaged)
	    continue;
	/* if invisible, ignore it */
	if (w->a.x + w->a.width < 1 || w->a.y + w->a.height < 1
	    || w->a.x >= root_width || w->a.y >= root_height)
	    continue;

	realize_window (dpy, w);

	if (clipChanged)
	    destroy_window_regions (dpy, w);

	if (!w->borderSize)
	    w->borderSize = border_size (dpy, w);
	if (!w->extents)
	    w->extents = win_extents (dpy, w);

	XFixesSetPictureClipRegion (dpy, rootBuffer, 0, 0, region);
	set_ignore (dpy, NextRequest (dpy));
	XFixesSubtractRegion (dpy, region, region, w->borderSize);
	backend->paint (dpy, w);
    }

    XFixesSetPictureClipRegion (dpy, rootBuffer, 0, 0, region);
    paint_root (dpy);
    XFixesDestroyRegion (dpy, region);
    if (rootBuffer != rootPicture)
    {
	backend->flip (dpy, updated);
    }
    XFixesDestroyRegion (dpy, updated);

    if (!dont_grab)
	XUngrabServer (dpy);
}

static void
add_damage (Display *dpy, XserverRegion damage)
{
    if (allDamage)
    {
	XFixesUnionRegion (dpy, allDamage, allDamage, damage);
	XFixesDestroyRegion (dpy, damage);
    }
    else
	allDamage = damage;
}

static void
repair_win (Display *dpy, win *w)
{
    XserverRegion   parts;

    if (!w->damaged)
    {
	parts = win_extents (dpy, w);
	set_ignore (dpy, NextRequest (dpy));
	XDamageSubtract (dpy, w->damage, None, None);
    }
    else
    {
	parts = XFixesCreateRegion (dpy, NULL, 0);
	set_ignore (dpy, NextRequest (dpy));
	XDamageSubtract (dpy, w->damage, None, parts);
	XFixesTranslateRegion (dpy, parts,
			       w->a.x + w->a.border_width,
			       w->a.y + w->a.border_width);
    }
    add_damage (dpy, parts);
    w->damaged = 1;
}

static void
map_win (Display *dpy, Window id)
{
    win		*w = find_win (dpy, id);

    if (!w)
	return;

    w->a.map_state = IsViewable;
    w->damaged = 0;
}

static void
render_forget_window (Display *dpy, win *w)
{
    if (w->render.picture)
    {
	set_ignore (dpy, NextRequest (dpy));
	XRenderFreePicture (dpy, w->render.picture);
	w->render.picture = None;
    }
}

static void
present_forget_window (Display *dpy, win *w)
{
    render_forget_window (dpy, w);
}

static void
finish_unmap_win (Display *dpy, win *w)
{
    w->damaged = 0;
    if (w->extents != None)
    {
	add_damage (dpy, w->extents);    /* destroys region */
	w->extents = None;
    }

    if (w->pixmap)
    {
	XFreePixmap (dpy, w->pixmap);
	w->pixmap = None;
    }

    backend->forget (dpy, w);

    destroy_window_regions (dpy, w);

    clipChanged = True;
}

static void
unmap_win (Display *dpy, Window id)
{
    win *w = find_win (dpy, id);
    if (!w)
	return;
    w->a.map_state = IsUnmapped;
    finish_unmap_win (dpy, w);
}

static void
add_win (Display *dpy, Window id, Window prev)
{
    win				*new = calloc (1, sizeof (win));
    win				**p;

    if (!new)
	return;
    if (prev)
    {
	for (p = &list; *p; p = &(*p)->next)
	    if ((*p)->id == prev)
		break;
    }
    else
	p = &list;
    new->id = id;
    set_ignore (dpy, NextRequest (dpy));
    if (!XGetWindowAttributes (dpy, id, &new->a))
    {
	free (new);
	return;
    }
    new->shape_bounds.x = new->a.x;
    new->shape_bounds.y = new->a.y;
    new->shape_bounds.width = new->a.width;
    new->shape_bounds.height = new->a.height;
    if (new->a.class != InputOnly)
    {
	new->damage = XDamageCreate (dpy, id, XDamageReportNonEmpty);
	XShapeSelectInput (dpy, id, ShapeNotifyMask);
    }

    new->next = *p;
    *p = new;
    if (new->a.map_state == IsViewable)
	map_win (dpy, id);
}

static void
restack_win (Display *dpy, win *w, Window new_above)
{
    Window  old_above;

    if (w->next)
	old_above = w->next->id;
    else
	old_above = None;
    if (old_above != new_above)
    {
	win **prev;

	/* unhook */
	for (prev = &list; *prev; prev = &(*prev)->next)
	    if ((*prev) == w)
		break;
	*prev = w->next;

	/* rehook */
	for (prev = &list; *prev; prev = &(*prev)->next)
	    if ((*prev)->id == new_above)
		break;

	w->next = *prev;
	*prev = w;
    }
}

static void
configure_win (Display *dpy, XConfigureEvent *ce)
{
    win		    *w = find_win (dpy, ce->window);
    XserverRegion   damage = None;

    if (!w)
    {
	if (ce->window == root)
	{
	    if (rootBuffer)
	    {
		XFreePixmap (dpy, rootPixmap);
		rootPixmap = None;
		XRenderFreePicture (dpy, rootBuffer);
		rootBuffer = None;
	    }
	    root_width = ce->width;
	    root_height = ce->height;
	}
	return;
    }
    damage = XFixesCreateRegion (dpy, NULL, 0);
    if (w->extents != None)
	XFixesCopyRegion (dpy, damage, w->extents);
    w->shape_bounds.x -= w->a.x;
    w->shape_bounds.y -= w->a.y;
    w->a.x = ce->x;
    w->a.y = ce->y;
    if (w->a.width != ce->width || w->a.height != ce->height)
    {
	if (w->pixmap)
	{
	    XFreePixmap (dpy, w->pixmap);
	    w->pixmap = None;
	    backend->forget (dpy, w);
	}
    }
    w->a.width = ce->width;
    w->a.height = ce->height;
    w->a.border_width = ce->border_width;
    w->a.override_redirect = ce->override_redirect;
    restack_win (dpy, w, ce->above);
    if (damage)
    {
	XserverRegion	extents = win_extents (dpy, w);
	XFixesUnionRegion (dpy, damage, damage, extents);
	XFixesDestroyRegion (dpy, extents);
	add_damage (dpy, damage);
    }
    w->shape_bounds.x += w->a.x;
    w->shape_bounds.y += w->a.y;
    if (!w->shaped)
    {
      w->shape_bounds.width = w->a.width;
      w->shape_bounds.height = w->a.height;
    }

    clipChanged = True;
}

static void
circulate_win (Display *dpy, XCirculateEvent *ce)
{
    win	    *w = find_win (dpy, ce->window);
    Window  new_above;

    if (!w)
	return;

    if (ce->place == PlaceOnTop)
	new_above = list->id;
    else
	new_above = None;
    restack_win (dpy, w, new_above);
    clipChanged = True;
}

static void
destroy_win (Display *dpy, Window id, Bool gone)
{
    win	**prev, *w;

    for (prev = &list; (w = *prev); prev = &w->next)
	if (w->id == id)
	{
	    if (gone)
		finish_unmap_win (dpy, w);
	    *prev = w->next;
	    backend->forget (dpy, w);
	    if (w->damage != None)
	    {
		set_ignore (dpy, NextRequest (dpy));
		XDamageDestroy (dpy, w->damage);
		w->damage = None;
	    }
	    free (w);
	    break;
	}
}

static struct backend render_backend = {
    render_realize_window,
    render_paint_window,
    render_forget_window,
    render_flip,
};

static struct backend present_backend = {
    present_realize_window,
    present_paint_window,
    present_forget_window,
    present_flip,
};

/*
static void
dump_win (win *w)
{
    printf ("\t%08lx: %d x %d + %d + %d (%d)\n", w->id,
	    w->a.width, w->a.height, w->a.x, w->a.y, w->a.border_width);
}


static void
dump_wins (void)
{
    win	*w;

    printf ("windows:\n");
    for (w = list; w; w = w->next)
	dump_win (w);
}
*/

static void
damage_win (Display *dpy, XDamageNotifyEvent *de)
{
    win	*w = find_win (dpy, de->drawable);

    if (!w)
	return;
    repair_win (dpy, w);
}

static void
shape_win (Display *dpy, XShapeEvent *se)
{
    win	*w = find_win (dpy, se->window);

    if (!w)
	return;

    if (se->kind == ShapeClip || se->kind == ShapeBounding)
    {
      XserverRegion region0;
      XserverRegion region1;

      clipChanged = True;

      region0 = XFixesCreateRegion (dpy, &w->shape_bounds, 1);

      if (se->shaped == True)
      {
	w->shaped = True;
	w->shape_bounds.x = w->a.x + se->x;
	w->shape_bounds.y = w->a.y + se->y;
	w->shape_bounds.width = se->width;
	w->shape_bounds.height = se->height;
      }
      else
      {
	w->shaped = False;
	w->shape_bounds.x = w->a.x;
	w->shape_bounds.y = w->a.y;
	w->shape_bounds.width = w->a.width;
	w->shape_bounds.height = w->a.height;
      }

      region1 = XFixesCreateRegion (dpy, &w->shape_bounds, 1);
      XFixesUnionRegion (dpy, region0, region0, region1); 
      XFixesDestroyRegion (dpy, region1);

      /* ask for repaint of the old and new region */
      paint_all (dpy, region0);
    }
}

static int
error (Display *dpy, XErrorEvent *ev)
{
    int	    o;
    const char    *name = NULL;
    static char buffer[256];

    if (should_ignore (dpy, ev->serial))
	return 0;

    if (ev->request_code == composite_opcode &&
	ev->minor_code == X_CompositeRedirectSubwindows)
    {
	fprintf (stderr, "Another composite manager is already running\n");
	exit (1);
    }

    o = ev->error_code - xfixes_error;
    switch (o) {
    case BadRegion: name = "BadRegion";	break;
    default: break;
    }
    o = ev->error_code - damage_error;
    switch (o) {
    case BadDamage: name = "BadDamage";	break;
    default: break;
    }
    o = ev->error_code - render_error;
    switch (o) {
    case BadPictFormat: name ="BadPictFormat"; break;
    case BadPicture: name ="BadPicture"; break;
    case BadPictOp: name ="BadPictOp"; break;
    case BadGlyphSet: name ="BadGlyphSet"; break;
    case BadGlyph: name ="BadGlyph"; break;
    default: break;
    }

    if (name == NULL)
    {
	buffer[0] = '\0';
	XGetErrorText (dpy, ev->error_code, buffer, sizeof (buffer));
	name = buffer;
    }

    fprintf (stderr, "error %d: %s request %d minor %d serial %lu\n",
	     ev->error_code, (strlen (name) > 0) ? name : "unknown",
	     ev->request_code, ev->minor_code, ev->serial);

/*    abort ();	    this is just annoying to most people */
    return 0;
}

static void
expose_root (Display *dpy, Window _root, XRectangle *rects, int nrects)
{
    XserverRegion  region = XFixesCreateRegion (dpy, rects, nrects);

    add_damage (dpy, region);
}

static int
ev_serial (XEvent *ev)
{
    if ((ev->type & 0x7f) != KeymapNotify)
	return ev->xany.serial;
    return NextRequest (ev->xany.display);
}

static char *
ev_name (XEvent *ev)
{
    static char	buf[128];
    switch (ev->type & 0x7f) {
    case Expose:
	return "Expose";
    case MapNotify:
	return "Map";
    case UnmapNotify:
	return "Unmap";
    case ReparentNotify:
	return "Reparent";
    case CirculateNotify:
	return "Circulate";
    default:
    	if (ev->type == damage_event + XDamageNotify)
	    return "Damage";
	else if (ev->type == xshape_event + ShapeNotify)
	    return "Shape";
	sprintf (buf, "Event %d", ev->type);
	return buf;
    }
}

static unsigned int
ev_window (XEvent *ev)
{
    switch (ev->type) {
    case Expose:
	return ev->xexpose.window;
    case MapNotify:
	return ev->xmap.window;
    case UnmapNotify:
	return ev->xunmap.window;
    case ReparentNotify:
	return ev->xreparent.window;
    case CirculateNotify:
	return ev->xcirculate.window;
    default:
    	if (ev->type == damage_event + XDamageNotify)
	    return ((XDamageNotifyEvent *) ev)->drawable;
	else if (ev->type == xshape_event + ShapeNotify)
	    return ((XShapeEvent *) ev)->window;
	return 0;
    }
}

static void
usage (char *program)
{
    fprintf (stderr, "usage: %s [options]\n", program);
    fprintf (stderr, "   -d <display>\n");
    fprintf (stderr, "   -f [sync|cmsg|none]\n");
    fprintf (stderr, "   -a      Automatic server-side compositing\n");
    fprintf (stderr, "   -e      Print event debugging\n");
    fprintf (stderr, "   -g      Don't grab/ungrab server around repaints\n");
    fprintf (stderr, "   -n      Client-side compositing with Render\n");
    fprintf (stderr, "   -p      Client-side compositing with Present\n");
    fprintf (stderr, "   -S      Enable synchronous operation\n");
    exit (1);
}

static Bool
register_cm (Display *dpy)
{
    Window w;
    Atom a;
    static char net_wm_cm[] = "_NET_WM_CM_Sxx";

    snprintf (net_wm_cm, sizeof (net_wm_cm), "_NET_WM_CM_S%d", scr);
    a = XInternAtom (dpy, net_wm_cm, False);

    w = XGetSelectionOwner (dpy, a);
    if (w != None)
	return False;

    w = XCreateSimpleWindow (dpy, RootWindow (dpy, scr), 0, 0, 1, 1, 0, None,
			     None);

    Xutf8SetWMProperties (dpy, w, "xcm", "xcm", NULL, 0, NULL, NULL, NULL);
    XSetSelectionOwner (dpy, a, w, 0);

    breadcrumb_window = w;

    return True;
}

static Bool
breadcrumb (Display *dpy)
{
    switch (syncMode) {
    case SyncCore:
	XSync (dpy, False);
	return False;
    case SyncCmsg: {
	XClientMessageEvent ev = {
	    .type = ClientMessage,
	    .window = root,
	    .message_type = xcm_breadcrumb_atom,
	    .format = 32,
	    .data.l[0] = 0,
	};

	XSendEvent (dpy, breadcrumb_window, False, 0, (XEvent *) &ev);
	return True;
	}
    case SyncNone:
	break;
    }
    return False;
}

static void
die(const char *message)
{
    fprintf (stderr, message);
    exit (1);
}

int
main (int argc, char **argv)
{
    Display	   *dpy;
    XEvent	    ev;
    Window	    root_return, parent_return;
    Window	    *children;
    unsigned int    nchildren;
    int		    i;
    XRenderPictureAttributes	pa;
    XRenderPictFormat *format;
    XRectangle	    *expose_rects = NULL;
    int		    size_expose = 0;
    int		    n_expose = 0;
    int		    p;
    int		    composite_major, composite_minor;
    char	    *display = NULL;
    int		    o;
    Bool	    synchronize;
    Bool	    blocked = False;

    while ((o = getopt (argc, argv, "ad:ef:gnpS")) != -1)
    {
	switch (o) {
	case 'a':
	    compMode = CompAuto;
	    break;
	case 'd':
	    display = optarg;
	    break;
	case 'e':
	    eventDebug = True;
	    break;
	case 'f':
	    if (!strcmp (optarg, "core"))
		syncMode = SyncCore;
	    else if (!strcmp (optarg, "cmsg"))
		syncMode = SyncCmsg;
	    else if (!strcmp (optarg, "none"))
		syncMode = SyncNone;
	    else
		usage (argv[0]);
	    break;
	case 'g':
	    dont_grab = True;
	    break;
	case 'n':
	    compMode = CompRender;
	    break;
	case 'p':
	    compMode = CompPresent;
	    break;
	case 'S':
	    synchronize = True;
	    break;
	default:
	    usage (argv[0]);
	    break;
	}
    }

    dpy = XOpenDisplay (display);
    if (!dpy)
	die ("Can't open display\n");

    XSetErrorHandler (error);
    if (synchronize)
	XSynchronize (dpy, 1);
    scr = DefaultScreen (dpy);
    root = RootWindow (dpy, scr);

    if (!XRenderQueryExtension (dpy, &render_event, &render_error))
	die ("No render extension\n");

    if (!XQueryExtension (dpy, COMPOSITE_NAME, &composite_opcode,
			  &composite_event, &composite_error))
	die ("No composite extension\n");

    XCompositeQueryVersion (dpy, &composite_major, &composite_minor);
    if (composite_major == 0 && composite_minor < 4)
	die ("Composite 0.4 or better is required\n");

    if (!XDamageQueryExtension (dpy, &damage_event, &damage_error))
	die ("No damage extension\n");

    if (!XFixesQueryExtension (dpy, &xfixes_event, &xfixes_error))
	die ("No XFixes extension\n");

    if (!XShapeQueryExtension (dpy, &xshape_event, &xshape_error))
	die ("No XShape extension\n");

    if (compMode == CompPresent)
    {
	if (!XPresentQueryExtension (dpy, &present_opcode, &present_event,
				     &present_error))
	    die ("No present extension\n");

	present_capabilities = XPresentQueryCapabilities (dpy, root);
    }

    if (syncMode == SyncCmsg)
    {
	xcm_breadcrumb_atom = XInternAtom (dpy, "XCM_BREADCRUMB", False);
    }

    if (!register_cm(dpy))
	die ("Another compositor is already running\n");

    if (compMode == CompRender)
	backend = &render_backend;
    if (compMode == CompPresent)
	backend = &present_backend;

    pa.subwindow_mode = IncludeInferiors;

    root_width = DisplayWidth (dpy, scr);
    root_height = DisplayHeight (dpy, scr);

    format = XRenderFindVisualFormat (dpy, DefaultVisual (dpy, scr));
    rootPicture = XRenderCreatePicture (dpy, root, format, CPSubwindowMode,
					&pa);
    allDamage = None;
    clipChanged = True;
    XGrabServer (dpy);
    if (compMode == CompAuto)
	XCompositeRedirectSubwindows (dpy, root, CompositeRedirectAutomatic);
    else
    {
	XCompositeRedirectSubwindows (dpy, root, CompositeRedirectManual);
	XSelectInput (dpy, root,
		      SubstructureNotifyMask|
		      ExposureMask|
		      StructureNotifyMask|
		      PropertyChangeMask);
	XShapeSelectInput (dpy, root, ShapeNotifyMask);
	XQueryTree (dpy, root, &root_return, &parent_return, &children,
		    &nchildren);
	for (i = 0; i < nchildren; i++)
	    add_win (dpy, children[i], i ? children[i-1] : None);
	XFree (children);
    }
    XUngrabServer (dpy);
    if (compMode != CompAuto)
	paint_all (dpy, None);
    for (;;)
    {
	/*	dump_wins (); */
	do {
	    XNextEvent (dpy, &ev);
	    if ((ev.type & 0x7f) != KeymapNotify)
		discard_ignore (dpy, ev.xany.serial);
	    if (eventDebug)
		printf ("event %10.10s serial 0x%08x window 0x%08x\n",
			ev_name(&ev), ev_serial (&ev), ev_window (&ev));
	    if (compMode != CompAuto) switch (ev.type) {
	    case ClientMessage:
		if (syncMode == SyncCmsg &&
		    ev.xclient.window == root &&
		    ev.xclient.message_type == xcm_breadcrumb_atom)
		    blocked = False;
		break;
	    case CreateNotify:
		add_win (dpy, ev.xcreatewindow.window, 0);
		break;
	    case ConfigureNotify:
		configure_win (dpy, &ev.xconfigure);
		break;
	    case DestroyNotify:
		destroy_win (dpy, ev.xdestroywindow.window, True);
		break;
	    case MapNotify:
		map_win (dpy, ev.xmap.window);
		break;
	    case UnmapNotify:
		unmap_win (dpy, ev.xunmap.window);
		break;
	    case ReparentNotify:
		if (ev.xreparent.parent == root)
		    add_win (dpy, ev.xreparent.window, 0);
		else
		    destroy_win (dpy, ev.xreparent.window, False);
		break;
	    case CirculateNotify:
		circulate_win (dpy, &ev.xcirculate);
		break;
	    case Expose:
		if (ev.xexpose.window == root)
		{
		    int more = ev.xexpose.count + 1;
		    if (n_expose == size_expose)
		    {
			if (expose_rects)
			{
			    expose_rects = realloc (expose_rects,
						    (size_expose + more) *
						    sizeof (XRectangle));
			    size_expose += more;
			}
			else
			{
			    expose_rects = malloc (more * sizeof (XRectangle));
			    size_expose = more;
			}
		    }
		    expose_rects[n_expose].x = ev.xexpose.x;
		    expose_rects[n_expose].y = ev.xexpose.y;
		    expose_rects[n_expose].width = ev.xexpose.width;
		    expose_rects[n_expose].height = ev.xexpose.height;
		    n_expose++;
		    if (ev.xexpose.count == 0)
		    {
			expose_root (dpy, root, expose_rects, n_expose);
			n_expose = 0;
		    }
		}
		break;
	    case PropertyNotify:
		for (p = 0; backgroundProps[p]; p++)
		{
		    Atom atom = XInternAtom (dpy, backgroundProps[p], False);
		    if (ev.xproperty.atom == atom)
		    {
			if (rootTile)
			{
			    XClearArea (dpy, root, 0, 0, 0, 0, True);
			    XRenderFreePicture (dpy, rootTile);
			    rootTile = None;
			    break;
			}
		    }
		}
		break;
	    default:
		if (ev.type == damage_event + XDamageNotify)
		{
		  damage_win (dpy, (XDamageNotifyEvent *) &ev);
		}
		else if (ev.type == xshape_event + ShapeNotify)
		{
		  shape_win (dpy, (XShapeEvent *) &ev);
		}
		break;
	    }
	} while (QLength (dpy));
	if (allDamage && !blocked && compMode != CompAuto)
	{
	    paint_all (dpy, allDamage);
	    blocked = breadcrumb (dpy);
	    allDamage = None;
	    clipChanged = False;
	}
    }
}
